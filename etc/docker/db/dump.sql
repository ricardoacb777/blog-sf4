-- Adminer 4.7.1 MySQL dump

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

SET NAMES utf8mb4;

INSERT INTO `Article` (`id`, `author_id`, `title`, `content`, `fecha`) VALUES
(1,	1,	'Test 1',	'Lorem ipsum sit amet dolor...',	'2019-02-06'),
(2,	1,	'Best practices with symfony4',	'Sit amet dolor, consecteur villlosaet',	'2019-02-15'),
(3,	1,	'Learning symfony4',	'What is Lorem Ipsum?\r\nLorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.',	'2019-02-07');


INSERT INTO `Author` (`id`, `name`) VALUES
(1,	'Ricardo Cermeño');

-- 2019-02-19 15:22:15