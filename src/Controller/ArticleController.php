<?php 

namespace App\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use App\Entity\Article;
use App\Entity\Author;
use App\Form\ArticleType;

class ArticleController extends AbstractController{
  /**
   * @Route("/", name="home")
   */
  public function homepage(){
    $articles = $this->listAll();
    return $this->render('article/index.html.twig', ["articles" => $articles]);
  }

  /**
   * @Route("/articles/{_action}/{id}",
   *  requirements = {
   *    "_action": "new|edit"
   *  }
   * )
   */
  public function formArticle($id = null, Request $request) {
    if($id){
      $article = $this->getDoctrine()->getRepository(Article::class)->find($id);
      $textSubmit = 'Save changes';
      $titleForm = 'Article edit';
    } else {
      $article = new Article();
      $textSubmit = 'Save';
      $titleForm = 'Article new';
    }
    $form = $this->createForm(ArticleType::class, $article);

    $form->handleRequest($request);

    if($form->isSubmitted() && $form->isValid()){
      $em = $this->getDoctrine()->getManager();
      $a = $form->getData();
      $em->persist($a);
      $em->flush();

      return $this->redirectToRoute('home');


    }
    
    return $this->render('article/form_page.html.twig',[
      'form' => $form->createView(),
      'textSubmit' => $textSubmit,
      'titleForm' => $titleForm
      ]);
  }

  /**
   * @Route("/articles/delete/{id}", name="delete_article")
   */
  public function removeArticle($id = null, Request $request) {
    $em = $this->getDoctrine()->getManager();
    $article = $this->getDoctrine()->getRepository(Article::class)->find($id);      
    $em->remove($article);
    $em->flush();
    return $this->redirectToRoute('home');
  }

  public function listAll() {
    return $this->getDoctrine()->getRepository(Article::class)->findAll();
  }

}


?>