# Initial variables
os ?= $(shell uname -s)

# Load custom setitngs
-include .env
export
PROVISION ?= docker
include etc/$(PROVISION)/makefile

install i: | build importdb open ## Perform install tasks (build, updatedb, index, test, and open). This is the default task

tag: ## Tag and push current branch. Usage make tag version=<semver>
	git tag -a $(version) -m "Version $(version)"
	git push origin $(version)

squash: branch := $(shell git rev-parse --abbrev-ref HEAD)
squash:
	git rebase -i $(shell git merge-base origin/$(branch) origin/master)
	git push -f

publish: test release checkoutlatesttag deploy ## Tag and deploy version. Registry authentication required. Usage: make publish
	git checkout master

preview review: container ?= app
preview review: version := $(shell git rev-parse --abbrev-ref HEAD)
preview review: ## Tag, deploy and push image of the current branch. Update service. Registry authentication required. Usage: make review
	make build container=
	make test container=
	make deploy
	make updateservice

push: branch := $(shell git rev-parse --abbrev-ref HEAD)
push: ## Review, add, commit and push changes using commitizen. Usage: make push
	git diff
	git add -A .
	@docker run --rm -it -e CUSTOM=true -v $(CURDIR):/app -v $(HOME)/.gitconfig:/root/.gitconfig aplyca/commitizen
	git pull origin $(branch)
	git push -u origin $(branch)

checkoutlatesttag:
	git fetch --prune origin "+refs/tags/*:refs/tags/*"
	git checkout $(shell git describe --always --abbrev=0 --tags)

# updatedb: ## Sync DB Schema from Doctrine entities
# 	make run command="bin/console doctrine:schema:update --force --complete --dump-sql"

# exportdb: exportdb.data exportdb.schema ## Export DB Schema and Data in separated files

# exportdb.data: ## Export DB data
# 	make exec container=db command="pg_dump -U app --no-owner --data-only --blobs --column-inserts --inserts --schema=public app > /docker-entrypoint-initdb.d/01-data.sql"

# exportdb.schema: ## Export DB Schema

importdb: importdb.migrate importdb.data

importdb.migrate: ##import database schema from file
	make exec command="bin/console doctrine:migrations:migrate --no-interaction"

importdb.data: ##import database from file
	make exec command="bin/console doctrine:database:import etc/docker/db/dump.sql"

h help: ## This help.
	@echo 'Usage: make <task>'
	@echo 'Default task: install'
	@echo
	@echo 'Tasks:'
	@awk 'BEGIN {FS = ":.*?## "} /^[a-zA-Z0-9., _-]+:.*?## / {printf "  \033[36m%-15s\033[0m %s\n", $$1, $$2}' $(MAKEFILE_LIST)

.DEFAULT_GOAL := install
.PHONY: all
